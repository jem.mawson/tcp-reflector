package au.com.loftinspace.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Jem
 */
public class FileLoader {

	/**
	 * @param filename
	 * 		The filename of the file to be loaded
	 * @return
	 * 		The contents of the file.
	 * @throws IOException
	 *      If any IOException is thrown by the underlying java.io package.
	 */
	public static String loadTextFile(String filename) throws IOException {
		StringBuffer returnBuffer = new StringBuffer(1024);
		char[] buffer = new char[1024];
		File file = new File(filename);
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		int charsRead = reader.read(buffer);
		while (charsRead != -1) {
			returnBuffer.append(buffer, 0, charsRead);
			charsRead = reader.read(buffer);
		}
		return returnBuffer.toString();
	}
}
