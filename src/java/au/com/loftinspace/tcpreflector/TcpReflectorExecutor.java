package au.com.loftinspace.tcpreflector;

import au.com.loftinspace.tcpreflector.packet.PacketListener;

import java.net.InetSocketAddress;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * The TcpReflectorExecutor is a convenience class for the starting and stopping
 * of TcpReflector instances. If you do not require thread level control of the TcpReflector
 * then this is the object for you.
 *
 * <br>Typical unit test usage would be:</br>
 * <font face="courier new" size="2" color="darkgreen">
 * TcpReflectorExecutor executor = new TcpReflectorExecutor(tcpReflector);<br>
 * executor.start(); // blocks until tcpReflector has initialised.<br>
 * // perform socket communications<br>
 * executor.stop(); // block until tcpReflector has terminated.<br>
 * try {<br>
 *     // perform socket communciations<br>
 *     fail("Expected SomeExpectedIOBasedException);<br>
 * } catch(SomeExpectedIOBasedException e) {<br>
 * }<br>
 * executor.start(); // blocks until tcpReflector has initialised again.<br>
 * // perform socket communications and ensure reconnection is OK.<br>
 * </font>
 *
 * Author: Jem Mawson
 * Date: 22/07/2004
 * Time: 22:10:35
 */
public class TcpReflectorExecutor {

    private TcpReflector reflector;
    private Thread reflectorThread;
    private int port;
    private InetSocketAddress destination;
    private List listeners;

    /**
     * Constructs a new instance of an executor.
     * @param listeningPort
     *      The local port number to connect to the TcpReflector.
     * @param destination
     *      The destination internet address for connections.
     * @param packetListeners
     *      A list of PacketListeners to be added to the TcpReflector.
     */
    public TcpReflectorExecutor(int listeningPort, InetSocketAddress destination,
                                List packetListeners) {

        this(listeningPort, destination);
        validateListenerInstances(packetListeners);
        this.listeners = packetListeners;
    }

    public TcpReflectorExecutor(int listeningPort, InetSocketAddress destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination address cannot be null");
        }
        this.port = listeningPort;
        this.destination = destination;
    }

    public synchronized void allowConnections(boolean allow) throws IOException {
        if (allow) {
            allowConnections();
        } else {
            disallowConnections();
        }
    }

    /**
     * Whether the reflector should allow
     * @param comms
     */
    public synchronized void allowResponsePassage(boolean comms) {
        if (reflector != null) {
            reflector.setResponseReflective(comms);
        }
    }

    private void allowConnections() throws IOException {
        if (reflector == null) {
            reflector = new TcpReflector(port, destination);
            addListeners();
            reflectorThread = new Thread(reflector, constructName());
            reflectorThread.setDaemon(true);
            reflectorThread.start();
        }

        while (!reflectorThread.isAlive()) {
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
            }
        }
    }

    private void disallowConnections() {
        if (reflector != null) {
            reflector.shutdown();
            try {
                reflectorThread.join();
            } catch (InterruptedException e) {
            } finally {
                reflector = null;
            }
        }
    }


    private String constructName() {
        StringBuffer name = new StringBuffer(64);
        name.append("TcpReflector_");
        name.append(System.currentTimeMillis());
        name.append(" {");
        name.append(reflector.getDestination().getHostName());
        name.append(":");
        name.append(reflector.getDestination().getPort());
        name.append("}");
        return name.toString();
    }

    private void validateListenerInstances(List packetListeners) {
        for (Iterator iterator = packetListeners.iterator(); iterator.hasNext();) {
            if (!(iterator.next() instanceof PacketListener)) {
                throw new IllegalArgumentException("PacketListeners list must contain only " +
                        "instances of PacketListener. " + packetListeners);
            }
        }
    }

    private void addListeners() {
        if (listeners != null) {
            for (Iterator iterator = listeners.iterator(); iterator.hasNext();) {
                reflector.addListener((PacketListener) iterator.next());
            }
        }
    }
}
