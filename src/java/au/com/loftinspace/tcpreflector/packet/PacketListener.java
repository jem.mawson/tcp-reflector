package au.com.loftinspace.tcpreflector.packet;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;


/**
 * PacketListeners listen for Packets and perform actions upon them.
 * @author Jeremy Mawson
 */
public abstract class PacketListener extends Thread {

    private boolean terminated;
    protected List packets;

    public PacketListener() {
        packets = Collections.synchronizedList(new LinkedList());
        terminated = false;
        start();
    }

    public final void run() {
        while (!terminated) {
            if (packets.isEmpty()) {
                sleep();
            } else {
                processPacket((Packet) packets.remove(0));
            }
        }
    }

    private void sleep() {
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            // ignored
        }
    }

    public final void add(Packet packet) {
        packets.add(packet);
    }

    public final void terminate() {
        terminated = true;
    }

    public String toString() {
        String state = null;
        if (isAlive()) {
            state = "started";
        } else if (terminated) {
            state = "finished";
        } else {
            state = "waiting to start";
        }
        return "PacketListener [" + state + "]";
    }

    /**
     * Processes a Packet. The implementing subclass may perform any action here. This method is
     * guaranteed to be called no more than once per Packet received.
     */
    public abstract void processPacket(Packet packet);
}
