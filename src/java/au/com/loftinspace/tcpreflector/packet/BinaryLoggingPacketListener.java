package au.com.loftinspace.tcpreflector.packet;

public class BinaryLoggingPacketListener extends PacketListener {

	/**
	 * Logs the content of the Packet as formatted binary
	 */
	public synchronized void processPacket(Packet packet) {
		byte[] bytes = packet.getBytes();
        System.out.println(packet.getFromAddress() + " -> " + packet.getToAddress() + " ("+packet.getSize()+" bytes)");
		for (int index = 0; index < bytes.length; index += 16) {
			System.out.println(formatNextSegment(index, bytes));
		}
        System.out.println();
	}

	private String formatNextSegment(int start, byte[] bytes) {
		StringBuffer segment = new StringBuffer(128);
		
		for (int index = start; index < start + 16; index++) {
			if (index < bytes.length) {
                byte aByte = bytes[index];
                segment.append(toHexString(aByte));
				segment.append(" ");
			} else {
				segment.append("   ");
			}
		}

        segment.append("   ");

		for (int index = start; index < start + 16; index++) {
			if (index < bytes.length) {
                byte aByte = bytes[index];
				segment.append(toChar(aByte));
			}
		}
		
		return segment.toString();
	}

    private char toChar(byte aByte) {
        return (aByte < 32) ? '.' : (char)aByte;
    }

    private static String toHexString(int anInt) {
        anInt += 128;
        String hexString = (anInt < 16) ? "0" + Integer.toHexString(anInt) : Integer.toHexString(anInt);
        return hexString;
    }
}
