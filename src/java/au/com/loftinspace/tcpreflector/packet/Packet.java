package au.com.loftinspace.tcpreflector.packet;

import java.net.InetSocketAddress;

/**
 * Wraps a single distinct TCP packet
 * @author Jeremy Mawson
 */
public class Packet {

    private byte[] bytes;
    private InetSocketAddress fromAddress;
    private InetSocketAddress toAddress;

    public Packet(byte[] bytes, int bytesRead, InetSocketAddress fromAddress, InetSocketAddress toAddress) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;

        if (bytesRead < 0) {
            bytesRead = 0;
        }
        if (bytesRead > bytes.length) {
            bytesRead = bytes.length;
        }
        this.bytes = new byte[bytesRead];
        System.arraycopy(bytes, 0, this.bytes, 0, bytesRead);
    }

    public byte[] getBytes() {
        return bytes;
    }

    public InetSocketAddress getFromAddress() {
        return fromAddress;
    }

    public InetSocketAddress getToAddress() {
        return toAddress;
    }

    public int hashCode() {
        return (new String(bytes)).hashCode();
    }

    public int getSize() {
        return bytes.length;
    }
}
