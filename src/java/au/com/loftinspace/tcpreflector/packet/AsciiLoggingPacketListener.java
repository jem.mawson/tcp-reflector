package au.com.loftinspace.tcpreflector.packet;

/**
 * @author Jem
 */
public class AsciiLoggingPacketListener extends PacketListener {

	/**
	 * Logs the content of the Packet as ASCII
	 */
	public synchronized void processPacket(Packet packet) {
        System.out.println(packet.getFromAddress() + " -> " + packet.getToAddress() + " ("+packet.getSize()+" bytes)");
		String message = new String(packet.getBytes());
		System.out.println(message);
	}
}
