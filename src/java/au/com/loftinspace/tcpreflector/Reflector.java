package au.com.loftinspace.tcpreflector;

import au.com.loftinspace.tcpreflector.io.InterceptorManager;
import au.com.loftinspace.tcpreflector.io.PacketInterceptor;
import au.com.loftinspace.tcpreflector.packet.PacketListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Reflects TCP packets delivered to and received from a network socket address.
 * @author Jem Mawson
 */
public class Reflector implements InterceptorManager {

	private Socket clientSocket;
	private Socket destinationSocket;
	private PacketInterceptor clientToDestinationHandler;
	private PacketInterceptor destinationToClientHandler;

    public Reflector(Socket clientSocket, Socket destinationSocket) throws IOException {
		this.clientSocket = clientSocket;
		this.destinationSocket = destinationSocket;
		InetSocketAddress clientAddress = new InetSocketAddress(clientSocket.getInetAddress(), clientSocket.getPort());
		InetSocketAddress destinationAddress = new InetSocketAddress(destinationSocket.getInetAddress(), destinationSocket.getPort());
        clientToDestinationHandler = new PacketInterceptor(clientSocket.getInputStream(), destinationSocket.getOutputStream(), this, clientAddress, destinationAddress);
		destinationToClientHandler = new PacketInterceptor(destinationSocket.getInputStream(), clientSocket.getOutputStream(), this, destinationAddress, clientAddress);
	}

	public void startReflecting() {
		String clientToDestinationThreadName = "PacketInterceptor " +
			clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " -> " +
			destinationSocket.getInetAddress() + ":" + destinationSocket.getPort();
		
		String destinationToClientThreadName = "PacketInterceptor " +
			clientSocket.getInetAddress() + ":" + clientSocket.getPort() + " <- " +
			destinationSocket.getInetAddress() + ":" + destinationSocket.getPort();
		
		new Thread(clientToDestinationHandler, clientToDestinationThreadName).start();
		new Thread(destinationToClientHandler, destinationToClientThreadName).start();
	}
	
	public void waitForCompletion() {
	    while (packetReceiversActive()) {
            try {
    	        Thread.sleep(250);
    	    } catch(InterruptedException ignored) {
    	    }
	    }
        terminateInterceptors();
	}
	
	public void terminateInterceptors() {
        if (destinationToClientHandler.isActive()) {
            destinationToClientHandler.terminate();
        }
        if (clientToDestinationHandler.isActive()) {
            clientToDestinationHandler.terminate();
        }
        terminateSocket(clientSocket);
        terminateSocket(destinationSocket);
	}

    private void terminateSocket(Socket socket) {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ignored) {
        }
    }

    private boolean packetReceiversActive() {
        return (clientToDestinationHandler.isActive()) && (destinationToClientHandler.isActive());
    }

	public void addListener(PacketListener listener) {
		clientToDestinationHandler.addListener(listener);
		destinationToClientHandler.addListener(listener);
	}
	
	public void removeListener(PacketListener listener) {
		clientToDestinationHandler.removeListener(listener);
		destinationToClientHandler.removeListener(listener);
	}

    public void setReflective(boolean reflective) {
        clientToDestinationHandler.setReflective(reflective);
        destinationToClientHandler.setReflective(reflective);
    }

    public void setResponseReflective(boolean reflective) {
        destinationToClientHandler.setReflective(reflective);
    }

    public void removeAllListeners() {
        clientToDestinationHandler.removeAllListeners();
        destinationToClientHandler.removeAllListeners();
    }
}
