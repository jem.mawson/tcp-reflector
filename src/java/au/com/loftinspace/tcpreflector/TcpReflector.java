package au.com.loftinspace.tcpreflector;

import au.com.loftinspace.tcpreflector.packet.PacketListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Accepts client socket connections and establishes reflector sessions for each new connection.
 * @author Jem Mawson
 */
public class TcpReflector implements Runnable {

    private static final int SERVER_SOCKET_TIMEOUT = 2000;

    private ServerSocket serverSocket;
    private InetSocketAddress destination;
    private List reflectors;
    private List listeners;
    private boolean reflective;
    private boolean terminated;
    private boolean connective;
    private boolean waitingForNewAcceptAttempt;
    private int listeningPort;

    public TcpReflector(int listeningPort, InetSocketAddress destination) throws IOException {
        this.listeningPort = listeningPort;
        this.destination = destination;
        this.reflectors = new ArrayList();
        this.listeners = new LinkedList();
        this.reflective = true;
        this.connective = true;
        initialiseServerSocket();
    }

    public void run() {
        try {
            while (!terminated) {
                acceptConnection();
            }
        } catch (Throwable t) {
            // ignored
        } finally {
            shutdown();
        }
    }

    /**
     * Whether this reflector should discard or redirect both incoming and outgoing packets.
     * @param reflective
     * 		<code>true</code> to redirect packets.<br>
     *  	<code>false</code> to discard them.
     */
    public void setReflective(boolean reflective) {
        Iterator iter = reflectors.iterator();
        while (iter.hasNext()) {
            Reflector reflector = (Reflector)iter.next();
            reflector.setReflective(reflective);
        }
        this.reflective = reflective;
    }

    /**
     * Whether this reflector should discard or redirect incoming packets.
     * @param reflective
     * 		<code>true</code> to deliver incoming packets.<br>
     *  	<code>false</code> to discard them.
     */
    public void setResponseReflective(boolean reflective) {
        Iterator iter = reflectors.iterator();
        while (iter.hasNext()) {
            Reflector reflector = (Reflector)iter.next();
            reflector.setResponseReflective(reflective);
        }
        this.reflective = reflective;
    }

    /**
     * Whether this reflector is redirecting packets.
     * @return
     * 		<code>true</code> if it redirects packets.<br>
     *  	<code>false</code> if it discards them.
     */
    public boolean isReflective() {
        return reflective;
    }

    /**
     * Returns an unmodifiable list of all PacketListeners associated with this instance.
     * @return
     *      A list of PacketListeners
     */
    public List getPacketListeners() {
        return Collections.unmodifiableList(listeners);
    }

    /**
     * Adds a packet listener to this instance. The listener will be called upon to take action
     * whenever a packet is transmitted in either direction. This action affects both existing
     * connections and new connections.
     */
    public void addListener(PacketListener listener) {
        listeners.add(listener);
        Iterator iter = reflectors.iterator();
        while (iter.hasNext()) {
            Reflector reflector = (Reflector) iter.next();
            reflector.addListener(listener);
        }
    }

    /**
     * Removes a PacketListener from this instance. The listener will no longer be called upon to
     * handle packets on conenctions originating from this instance. This action affects both
     * existing connections and new connections.
     */
    public void removeListener(PacketListener listener) {
        listeners.remove(listener);
        Iterator iter = reflectors.iterator();
        while (iter.hasNext()) {
            Reflector reflector = (Reflector) iter.next();
            reflector.removeListener(listener);
        }
    }

    /**
     * Removes all PacketListeners from this instance. None of the packets on connections
     * originating from this instance will be handled by any PacketListeners until such time as a
     * new PacketListener is appended. This action affects both existing connections and new
     * connections.
     */
    public void removeAllListeners() {
        listeners.clear();
        Iterator iter = reflectors.iterator();
        while (iter.hasNext()) {
            Reflector reflector = (Reflector) iter.next();
            reflector.removeAllListeners();
        }
    }

    /**
     * Get the destination internet address for connections.
     * @return
     *      The destination internet address for connections.
     */
    public InetSocketAddress getDestination() {
        return destination;
    }

    /**
     * Set the destination internet address for future connections. This does not affect currently
     * active connections.
     * @param destination
     *      The destination for future connections.
     */
    public void setDestination(InetSocketAddress destination) {
        this.destination = destination;
    }

    /**
     * Returns whether this instance will accept new socket connections. If this is false then
     * connection attempts will result in an IOException.
     * @return
     *      Whether this instance will accept new socket connections.
     */
    public boolean isConnective() {
        return connective;
    }

    /**
     * Sets whether this instance will accept connections. This method will quick return and does
     * not guarantee that a change from true to false will take effect immediately.
     *
     * until the
     * instance state has been effectively changed. In practice this may mean waiting until any
     * current socket accept attempt has timed out.
     * @param connective
     *      Whether this instance should accept connections.
     * @throws IOException
     *      If a problem is encountered establishing or shutting down the underlying ServerSocket
     *      connection.
     */
    public void setConnective(boolean connective) throws IOException {
        if (this.connective == connective) {
            return;
        }
        this.connective = connective;
        waitingForNewAcceptAttempt = true;

        if (connective) {
            initialiseServerSocket();
            blockUntilNewConnectAttempt();
        } else {
            blockUntilNewConnectAttempt();
            shutdownServerSocket();
        }
    }

    /**
     * Causes the reflector to terminate all processing. The underlying socket
     * connection will be lost. This action is unrecoverable.
     */
    public synchronized void shutdown() {
        if (terminated) {
            return;
        }
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ignored) {
            }
            serverSocket = null;
        }
        Iterator iter = reflectors.iterator();
        while (iter.hasNext()) {
            Reflector reflector = (Reflector)iter.next();
            reflector.terminateInterceptors();
        }
        terminated = true;
    }

    private void acceptConnection() throws IOException {
        if (!connective) {
            sleep(SERVER_SOCKET_TIMEOUT);
            return;
        }

        Socket clientSocket;
        try {
            waitingForNewAcceptAttempt = false;
            clientSocket = serverSocket.accept();
        } catch (SocketTimeoutException e) {
            return;
        }
        Socket destinationSocket = new Socket(destination.getHostName(), destination.getPort());
        Reflector reflector = new Reflector(clientSocket, destinationSocket);
        reflector.setReflective(reflective);
        Iterator iter = listeners.iterator();
        while (iter.hasNext()) {
            reflector.addListener((PacketListener) iter.next());
        }
        reflector.startReflecting();
        reflectors.add(reflector);
    }

    private void initialiseServerSocket() throws IOException {
        serverSocket = new ServerSocket(listeningPort);
        serverSocket.setSoTimeout(SERVER_SOCKET_TIMEOUT);
    }

    private void shutdownServerSocket() throws IOException {
        serverSocket.close();
        serverSocket = null;
    }

    private void blockUntilNewConnectAttempt() {
        long endtime = System.currentTimeMillis() + (SERVER_SOCKET_TIMEOUT * 2);
        while (waitingForNewAcceptAttempt && (System.currentTimeMillis() < endtime)) {
            sleep(100L);
        }

        // This ServerSocket implementation ignores the SO_TIMEOUT parameter, force it down.
        if (waitingForNewAcceptAttempt) {
            try {
                new Socket("127.0.0.1", serverSocket.getLocalPort());
            } catch (IOException e) {
            }

            endtime = System.currentTimeMillis() + (SERVER_SOCKET_TIMEOUT * 2);
            while (waitingForNewAcceptAttempt && (System.currentTimeMillis() < endtime)) {
                sleep(100L);
            }
        }
    }

    private void sleep(long duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
        }
    }
}
