package au.com.loftinspace.tcpreflector.io;

/**
 * Author: Jem
 * Date: 14/02/2004
 * Time: 14:55:52
 */
public interface InterceptorManager {
    public void terminateInterceptors();
}
