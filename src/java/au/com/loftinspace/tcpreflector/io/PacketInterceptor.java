package au.com.loftinspace.tcpreflector.io;

import au.com.loftinspace.tcpreflector.packet.Packet;
import au.com.loftinspace.tcpreflector.packet.PacketListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Accepts input from the input stream and redirects it to the output stream
 * @author Jem
 */
public class PacketInterceptor implements Runnable {

    private static final int MAX_PACKET_SIZE = 4096;

    private LinkedList listeners;
    private boolean active;
    private byte[] buffer;
    private InputStream inputStream;
    private OutputStream outputStream;
    private InterceptorManager interceptorManager;
    private boolean reflective;
    private InetSocketAddress toAddress;
    private InetSocketAddress fromAddress;

    public PacketInterceptor(InputStream inputStream, OutputStream outputStream, InterceptorManager interceptorManager, InetSocketAddress fromAddress, InetSocketAddress toAddress) {
        this.outputStream = outputStream;
        this.inputStream = inputStream;
        this.interceptorManager = interceptorManager;
        this.listeners = new LinkedList();
        this.buffer = new byte[MAX_PACKET_SIZE];
        this.active = true;
        this.reflective = true;
        this.toAddress = toAddress;
        this.fromAddress = fromAddress;
    }

    public void addListener(PacketListener listener) {
        listeners.add(listener);
    }

    public void removeListener(PacketListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    public int getListenerCount() {
        return listeners.size();
    }

    public void run() {
        try {
            while (active) {
                redirectPacket();
            }
        } catch (IOException e) {
            interceptorManager.terminateInterceptors();
        }
    }

    public void terminate() {
        active = false;
        closeStream(inputStream);
        closeStream(outputStream);
    }

    public boolean isActive() {
        return active;
    }

    private void redirectPacket() throws IOException {
        int bytesRead = inputStream.read(buffer, 0, MAX_PACKET_SIZE);
        if (bytesRead == -1) {
            interceptorManager.terminateInterceptors();
        } else {
            if (reflective) {
                outputStream.write(buffer, 0, bytesRead);
                outputStream.flush();
                Packet packet = new Packet(buffer, bytesRead, fromAddress, toAddress);
                adviseListeners(packet);
            }
        }
    }

    private void closeStream(InputStream inputStream) {
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException ignored) {
        }
    }

    private void closeStream(OutputStream outputStream) {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException ignored) {
        }
    }

    private void adviseListeners(Packet packet) {
        Iterator iter = listeners.iterator();
        while (iter.hasNext()) {
            PacketListener packetListener = (PacketListener) iter.next();
            packetListener.add(packet);
        }
    }

    public void setReflective(boolean reflective) {
        this.reflective = reflective;
    }
}
