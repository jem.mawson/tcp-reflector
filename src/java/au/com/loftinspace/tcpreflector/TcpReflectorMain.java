package au.com.loftinspace.tcpreflector;

import au.com.loftinspace.tcpreflector.packet.AsciiLoggingPacketListener;
import au.com.loftinspace.tcpreflector.packet.BinaryLoggingPacketListener;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Author: Jem
 * Date: 17/02/2004
 * Time: 22:57:11
 */
public class TcpReflectorMain {
    private static final String TAG_PORT = "-port";
    private static final String TAG_HELP = "-help";
    private static final String TAG_DESTINATION = "-dest";
    private static final String TAG_FORMAT = "-format";
    private static final String VALUE_ASCII = "ascii";
    private static final String VALUE_BINARY = "binary";
    private static final String VALUE_NONE = "none";

    private static int listenPort;
    private static InetSocketAddress destination;
    private static String listenerType;

    public static void main(String[] args) throws IOException {
        parseArguments(args);
        TcpReflector reflectorSpawner = new TcpReflector(listenPort, destination);
        addPacketListener(reflectorSpawner);
        printStartedMessage();
        reflectorSpawner.run();
    }

    private static void parseArguments(String[] args) {
        int index = 0;

        while (index < args.length) {
            if (args[index].equals(TAG_HELP)) {
                printUsageAndExit(0);

            } else if (args[index].equals(TAG_PORT)) {
                ensureMoreArgumentsExist(args, index);
                listenPort = parseInt(args[++index]);

            } else if (args[index].equals(TAG_DESTINATION)) {
                ensureMoreArgumentsExist(args, index);
                destination = parseAddress(args[++index]);

            } else if (args[index].equals(TAG_FORMAT)) {
                ensureMoreArgumentsExist(args, index);
                listenerType = parseListenerType(args[++index]);

            } else {
                printUsageAndExit(-1);
            }

            index++;
        }

        if ((listenPort == 0) || (destination == null)) {
            printUsageAndExit(-1);
        }
    }

    private static String parseListenerType(String argument) {
        if (!((VALUE_ASCII.equalsIgnoreCase(argument)) || (VALUE_BINARY.equalsIgnoreCase(argument)) || (VALUE_NONE.equalsIgnoreCase(argument)))) {
            printUsageAndExit(-1);
        }
        return argument;
    }

    private static InetSocketAddress parseAddress(String argument) {
        String[] hostAndPort = argument.split(":");
        if (hostAndPort.length != 2) {
            printUsageAndExit(-1);
        }
        return new InetSocketAddress(hostAndPort[0], parseInt(hostAndPort[1]));
    }

    private static int parseInt(String argument) {
        int value = 0;
        try {
            value = Integer.parseInt(argument);
        } catch (NumberFormatException e) {
            printUsageAndExit(-1);
        }
        return value;
    }

    private static void ensureMoreArgumentsExist(String[] args, int index) {
        if (args.length <= index) {
            printUsageAndExit(-1);
        }
    }

    private static void addPacketListener(TcpReflector reflectorSpawner) {
        if (VALUE_ASCII.equals(listenerType)) {
            reflectorSpawner.addListener(new AsciiLoggingPacketListener());
        }
        if (VALUE_BINARY.equals(listenerType)) {
            reflectorSpawner.addListener(new BinaryLoggingPacketListener());
        }
    }

    private static void printStartedMessage() {
        System.out.println("TcpReflectorMain running on port " + listenPort + ", redirecting to " + destination + " and using logger [" + listenerType + "]");
    }

    private static void printUsageAndExit(int exitCode) {
        System.out.println("TcpReflectorMain usage:");
        System.out.println("java -port {localPortNumber} -dest {destinationAddress}:{destinationPort} [-format {binary|ascii|none}]");
        System.out.println();
        System.out.println("localPortNumber:    TcpReflectorMain will listen for socket connections at this port");
        System.out.println("destinationAddress: Host to reflect all packets to and from");
        System.out.println("destinationPort:    Port to reflect all packets to and from");
        System.out.println("binary|ascii|none:  How to log packets. Either ASCII, binary or not at all");
        System.exit(exitCode);
    }
}
