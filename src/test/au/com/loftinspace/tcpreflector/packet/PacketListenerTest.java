package au.com.loftinspace.tcpreflector.packet;

import junit.framework.TestCase;

import java.util.HashSet;
import java.net.InetSocketAddress;

/**
 * Author: Jem
 * Date: 14/02/2004
 * Time: 16:05:46
 */
public class PacketListenerTest extends TestCase {

    public void testProcessesPackets() throws Exception {
        byte distinctPacketCount = Byte.MAX_VALUE;
        TestPacketListener testPacketListener = new TestPacketListener();
        for (byte b = Byte.MIN_VALUE; b < distinctPacketCount; b++) {
            testPacketListener.add(new Packet(new byte[]{b}, 1, new InetSocketAddress(1), new InetSocketAddress(1)));
        }
        Thread.sleep(500);
        int expectedCount = ((int) Byte.MAX_VALUE) - ((int) Byte.MIN_VALUE);
        int actualCount = testPacketListener.getDistinctPacketCount();
        assertTrue(actualCount == expectedCount);
    }

    private class TestPacketListener extends PacketListener {
        private HashSet packetsSet = new HashSet();

        public void processPacket(Packet packet) {
            packetsSet.add(packet);
        }

        public int getDistinctPacketCount() {
            return packetsSet.size();
        }
    }
}
