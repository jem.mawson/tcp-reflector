package au.com.loftinspace.tcpreflector.packet;

import junit.framework.TestCase;

import java.net.InetSocketAddress;

/**
 * Author: Jem
 * Date: 14/02/2004
 * Time: 15:44:08
 */
public class PacketTest extends TestCase {

    private InetSocketAddress fromAddress = new InetSocketAddress("localhost", 80);
    private InetSocketAddress toAddress = new InetSocketAddress("localhost", 81);

    public void testEmptyByteArray() throws Exception {
        Packet packet = new Packet(new byte[0], 0, fromAddress, toAddress);
        assertNotNull(packet);
        assertTrue(packet.getBytes().length == 0);
    }

    public void testPacketSize() throws Exception {
        byte[] contents = new byte[255];
        Packet packet = new Packet(contents, 50, fromAddress, toAddress);
        assertTrue(packet.getBytes().length == 50);
    }

    public void testInvalidConstructorCalls() throws Exception {
        Packet packet = new Packet(new byte[255], -50, fromAddress, toAddress);
        assertTrue(packet.getBytes().length == 0);

        packet = new Packet(new byte[0], 255, fromAddress, toAddress);
        assertTrue(packet.getBytes().length == 0);

        packet = new Packet(new byte[255], 50, fromAddress, toAddress);
        assertTrue(packet.getBytes().length == 50);
    }

    public void testGetAddresses() throws Exception {
        Packet packet = new Packet(new byte[1], 1, fromAddress, toAddress);
        assertEquals(fromAddress, packet.getFromAddress());
        assertEquals(toAddress, packet.getToAddress());
    }
}
