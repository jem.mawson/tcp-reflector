package au.com.loftinspace.tcpreflector.test;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Author: Jem Mawson
 * Date: 1/08/2004
 * Time: 22:30:16
 */
public class ByteCachingThread extends Thread {
    private static final Logger logger = Logger.getLogger(ByteCachingThread.class);
    private Socket socket;
    private StringBuffer cache;

    public ByteCachingThread(Socket socket) {
        super("ByteCachingThread");
        this.socket = socket;
        cache = new StringBuffer(255);
        setDaemon(true);
    }

    public String serveCache() {
        synchronized (cache) {
            String result = cache.toString();
            cache.delete(0, cache.length());
            return result;
        }
    }

    public boolean waitForResponse(String expectedResponse, long timeout) {
        long endTime = System.currentTimeMillis() + timeout;
        while ((!cache.toString().startsWith(expectedResponse)) &&
                (System.currentTimeMillis() < endTime)) {

            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
            }
        }
        return (serveCache().startsWith(expectedResponse));
    }

    public void run() {
        try {
            InputStream input = socket.getInputStream();

            byte[] response = new byte[255];
            int bytesRead = input.read(response);
            while (bytesRead != -1) {
                synchronized (cache) {
                    logger.debug("bytesRead = " + bytesRead);
                    cache.append(new String(response, 0, bytesRead));
                }
                bytesRead = input.read(response);
            }
            socket.close();
        } catch (IOException e) {
        }
    }
}
