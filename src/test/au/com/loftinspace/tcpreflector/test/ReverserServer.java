package au.com.loftinspace.tcpreflector.test;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;

/**
 * Server for testing purposes. Accepts String input from clients and returns it reversed.
 * Author: Jem
 * Date: 14/06/2004
 * Time: 22:46:52
 */
public class ReverserServer implements Runnable {

    private ServerSocket server;
    private int listenPort;

    public ReverserServer(int listenPort) throws IOException {
        this.listenPort = listenPort;
        server = new ServerSocket(listenPort);
        server.setSoTimeout(50000);
    }

    public int getListenPort() {
        return listenPort;
    }

    public void run() {
        Socket client = null;
        try {
            client = server.accept();
            byte[] input = new byte[255];
            int bytesRead = client.getInputStream().read(input);
            if (bytesRead != -1) {
                String message = new String(input, 0, bytesRead);
                message = (new StringBuffer(message)).reverse().toString();
                client.getOutputStream().write(message.getBytes());
                client.getOutputStream().flush();
            }
        } catch (IOException e) {
        } finally {
            try {
                if (client != null) {
                    client.close();
                }
                if (server != null) {
                    server.close();
                }
            } catch (IOException ignored) {
            }
        }
    }
}
