package au.com.loftinspace.tcpreflector.test;

import au.com.loftinspace.tcpreflector.packet.PacketListener;
import au.com.loftinspace.tcpreflector.packet.Packet;

/**
 * Author: Jem
 * Date: 15/06/2004
 * Time: 22:41:45
 */
public class MessageCountingPacketListener extends PacketListener {

    private int packetCount = 0;

    public void processPacket(Packet packet) {
        packetCount++;
    }

    public void resetCount() {
        packetCount = 0;
    }

    public int getPacketCount() {
        return packetCount;
    }

    public void assertPacketCount(int count, long timeout) throws Exception {
        long end = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < end) {
            if (count == packetCount) {
                return;
            }
            Thread.sleep(100L);
        }
        throw new AssertionError("Packet count did not reach " + count + " within " + timeout + "ms");
    }
}
