package au.com.loftinspace.tcpreflector.test;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Server for testing purposes. Accepts and counts client connections.
 * Author: Jem Mawson
 * Date: 14/06/2004
 * Time: 22:45:01
 */
public class ConnectionCounterServer implements Runnable {

    private ServerSocket server;
    private int listenPort;
    private boolean active;
    private int actualConnectionCount;

    public ConnectionCounterServer(int listenPort) throws IOException {
        this.listenPort = listenPort;
        active = true;
        actualConnectionCount = 0;
        server = new ServerSocket(listenPort);
        server.setSoTimeout(5000);
    }

    public int getListenPort() {
        return listenPort;
    }

    public void run() {
        while (active) {
            try {
                server.accept();
                actualConnectionCount++;
            } catch (IOException ignored) {
            }
        }
    }

    public int getActualConnectionCount() {
        return actualConnectionCount;
    }

    public void terminate() {
        active = false;
    }
}
