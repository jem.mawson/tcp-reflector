package au.com.loftinspace.tcpreflector.test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * A test server that does not close the socket connection until it is advised to.
 * Author: Jem Mawson
 * Date: 20/07/2004
 * Time: 22:01:51
 */
public class LongRunningServer implements Runnable {

    private int listenPort;
    private ServerSocket server;
    private Socket client;

    public LongRunningServer(int listenPort) throws IOException {
        this.listenPort = listenPort;
        server = new ServerSocket(listenPort);
        server.setSoTimeout(50000);
    }

    public int getListenPort() {
        return listenPort;
    }

    public void run() {
        client = null;
        try {
            client = server.accept();
            // this must be instantiated as a daemon thread or your test wont end.
            byte[] input = new byte[255];
            while (true) {
                int bytesRead = client.getInputStream().read(input);
                if (bytesRead != -1) {
                    String message = new String(input, 0, bytesRead);
                    message = message.toLowerCase();
                    client.getOutputStream().write(message.getBytes());
                    client.getOutputStream().flush();
                }
            }
        } catch (IOException e) {
        } finally {
            try {
                if (client != null) {
                    client.close();
                }
                if (server != null) {
                    server.close();
                }
            } catch (IOException ignored) {
            }
        }
    }
}
