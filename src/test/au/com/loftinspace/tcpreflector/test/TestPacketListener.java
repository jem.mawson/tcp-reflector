package au.com.loftinspace.tcpreflector.test;

import au.com.loftinspace.tcpreflector.packet.Packet;
import au.com.loftinspace.tcpreflector.packet.PacketListener;

/**
 * Author: Jem Mawson
 * Date: 15/06/2004
 * Time: 22:56:30
 */
public class TestPacketListener extends PacketListener {
    private Packet packet;

    public void processPacket(Packet packet) {
        this.packet = packet;
    }

    public Packet getPacket() {
        return packet;
    }
}