package au.com.loftinspace.tcpreflector.test;

import au.com.loftinspace.tcpreflector.packet.Packet;
import au.com.loftinspace.tcpreflector.packet.PacketListener;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Jem
 * Date: 15/06/2004
 * Time: 22:41:45
 */
public class DestinationCountingPacketListener extends PacketListener {

    private Map destinationTallies = new HashMap();

    public void processPacket(Packet packet) {
        Integer count = (Integer) destinationTallies.get(packet.getToAddress());
        if (count == null) {
            count = new Integer(1);
        } else {
            count = new Integer(count.intValue() + 1);
        }
        destinationTallies.put(packet.getToAddress(), count);
    }

    public void assertPacketCount(InetSocketAddress destination, int count, long timeout) throws Exception {
        long end = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < end) {
            Integer messageCount = (Integer) destinationTallies.get(destination);
            if ((messageCount != null) && (count == messageCount.intValue())) {
                return;
            }
            Thread.sleep(100L);
        }
        throw new AssertionError("Packet count for " + destination + " did not reach " + count + " within " + timeout + "ms");
    }
}
