package au.com.loftinspace.tcpreflector;

import au.com.loftinspace.tcpreflector.packet.BinaryLoggingPacketListener;
import au.com.loftinspace.tcpreflector.packet.PacketListener;
import au.com.loftinspace.tcpreflector.test.ByteCachingThread;
import au.com.loftinspace.tcpreflector.test.ConnectionCounterServer;
import au.com.loftinspace.tcpreflector.test.LongRunningServer;
import junit.framework.TestCase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

/**
 * Author: Jem Mawson
 * Date: 22/07/2004
 * Time: 23:02:14
 */
public class TcpReflectorExecutorTest extends TestCase {

    public void testConnectPermission() throws Exception {

        int reflectorPort = 9444;
        int serverPort = 9445;

        // Start the server that will accept our test connections.
        ConnectionCounterServer server = createCounterServer(serverPort);

        // Start the executor thread that controls if we're connected.
        TcpReflectorExecutor executor = new TcpReflectorExecutor(reflectorPort,
                new InetSocketAddress("localhost", server.getListenPort()),
                Arrays.asList(new PacketListener[] {new BinaryLoggingPacketListener()}));
        executor.allowConnections(true);

        // Create a new connection and assert it was successful
        new Socket("localhost", reflectorPort);
        Thread.sleep(500L);
        assertEquals(1, server.getActualConnectionCount());

        // Disallow connections and attempt a new connection.
        executor.allowConnections(false);
        try {
            new Socket("localhost", reflectorPort);
            fail("Expected java.net.ConnectException");
        } catch (ConnectException e) {
        }
        Thread.sleep(500L);
        assertEquals(1, server.getActualConnectionCount());

        // Reallow connections and assert that we can successfully connect.
        executor.allowConnections(true);
        new Socket("localhost", reflectorPort);
        Thread.sleep(500L);
        assertEquals(2, server.getActualConnectionCount());
    }

    public void testNoResponse() throws Exception {
        int reflectorPort = 9448;
        int serverPort = 9455;

        // Start the server that will accept our test connections.
        LongRunningServer server = createLongRunningServer(serverPort);

        // Start the executor thread that controls if we're connected.
        TcpReflectorExecutor executor = new TcpReflectorExecutor(reflectorPort,
                new InetSocketAddress("localhost", server.getListenPort()),
                Arrays.asList(new PacketListener[] {new BinaryLoggingPacketListener()}));
        executor.allowConnections(true);

        Socket socket = new Socket("localhost", reflectorPort);
        ByteCachingThread responseCache = new ByteCachingThread(socket);
        responseCache.start();

        OutputStream output = socket.getOutputStream();

        executor.allowResponsePassage(true);
        output.write("ECHO".getBytes());
        output.flush();
        assertTrue(responseCache.waitForResponse("echo", 1000L));

        executor.allowResponsePassage(false);
        output.write("ECHO".getBytes());
        output.flush();
        assertFalse(responseCache.waitForResponse("echo", 1000L));

        executor.allowResponsePassage(true);
        output.write("ECHO".getBytes());
        output.flush();
        assertTrue(responseCache.waitForResponse("echo", 1000L));

        socket.close();
    }

    private ConnectionCounterServer createCounterServer(int startPort) throws IOException {
        ConnectionCounterServer server = null;
        boolean success = false;
        while (!success) {
            try {
                server = new ConnectionCounterServer(++startPort);
                success = true;
            } catch (BindException e) {
            }
        }
        Thread serverThread = new Thread(server);
        serverThread.setDaemon(true);
        serverThread.start();
        return server;
    }

    private LongRunningServer createLongRunningServer(int startPort) throws IOException {
        LongRunningServer server = null;
        boolean success = false;
        while (!success) {
            try {
                server = new LongRunningServer(++startPort);
                success = true;
            } catch (BindException e) {
            }
        }
        Thread serverThread = new Thread(server);
        serverThread.setDaemon(true);
        serverThread.start();
        return server;
    }

    private void testResponseFails(OutputStream output, final InputStream input) throws IOException {
        output.write("hello?".getBytes());
        output.flush();

        Thread readerThread = new Thread() {
            public boolean responded = false;
            public void run() {
                byte[] response = new byte[7];
                try {
                    input.read(response);
                    System.out.println("input = " + response);
                } catch (IOException e) {
                }
                assertFalse("Input was read, but packet loss was turned on", response[0] == '?');
            }
        };
        readerThread.setDaemon(true);
        readerThread.start();

        long endtime = System.currentTimeMillis() + 3000L;
        while ((System.currentTimeMillis() < endtime) && (readerThread.isAlive()))
        {
            try {
                Thread.sleep(250L);
            } catch (InterruptedException e) {
            }
        }
    }

    private void testResponse(OutputStream output, InputStream input) throws IOException {
        output.write("ECHO".getBytes());
        output.flush();
        byte[] response = new byte[4];
        int e = input.read();
        int c = input.read();
        int h = input.read();
        int o = input.read();
        assertTrue(((char) e) == 'e');
        assertTrue(((char) c) == 'c');
        assertTrue(((char) h) == 'h');
        assertTrue(((char) o) == 'o');
    }
}
