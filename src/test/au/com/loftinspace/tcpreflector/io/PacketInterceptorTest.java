package au.com.loftinspace.tcpreflector.io;

import au.com.loftinspace.tcpreflector.packet.Packet;
import au.com.loftinspace.tcpreflector.test.TestPacketListener;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.InetSocketAddress;

/**
 * Author: Jem
 * Date: 14/02/2004
 * Time: 14:28:20
 */
public class PacketInterceptorTest extends TestCase implements InterceptorManager {

    private PacketInterceptor packetInterceptor;

    public void testPacketRedirect() throws Exception {
        String testInput = "this is the test input";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(testInput.getBytes());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(testInput.length());

        packetInterceptor = new PacketInterceptor(inputStream, outputStream, this, new InetSocketAddress(1), new InetSocketAddress(1));
        assertTrue(packetInterceptor.isActive());

        Thread interceptorThread = new Thread(packetInterceptor, "PacketInterceptor");
        interceptorThread.setDaemon(true);
        interceptorThread.start();
        Thread.sleep(500);
        packetInterceptor.terminate();
        Thread.sleep(500);

        assertFalse(packetInterceptor.isActive());
        assertEquals(testInput, outputStream.toString());
    }

    public void testInterceptorAdvisesListeners() throws Exception {

        TestPacketListener testPacketListener = new TestPacketListener();

        String testInput = "this is the test input";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(testInput.getBytes());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(testInput.length());

        packetInterceptor = new PacketInterceptor(inputStream, outputStream, this, new InetSocketAddress(1), new InetSocketAddress(1));
        packetInterceptor.addListener(testPacketListener);
        Thread interceptorThread = new Thread(packetInterceptor, "PacketInterceptor");
        interceptorThread.setDaemon(true);
        interceptorThread.start();
        Thread.sleep(500);
        Packet packet = testPacketListener.getPacket();
        assertNotNull(packet);
        assertEquals(testInput, new String(packet.getBytes()));
        packetInterceptor.terminate();
    }

    public void testPacketListenerAdditivity() throws Exception {
        TestPacketListener listener = new TestPacketListener();

        String testInput = "this is the test input";
        ByteArrayInputStream inputStream = new ByteArrayInputStream(testInput.getBytes());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(testInput.length());
        packetInterceptor = new PacketInterceptor(inputStream, outputStream, this, new InetSocketAddress(1), new InetSocketAddress(1));

        assertEquals(0, packetInterceptor.getListenerCount());
        packetInterceptor.addListener(listener);
        assertEquals(1, packetInterceptor.getListenerCount());
        packetInterceptor.addListener(listener);
        assertEquals(2, packetInterceptor.getListenerCount());
        packetInterceptor.addListener(listener);
        assertEquals(3, packetInterceptor.getListenerCount());
        packetInterceptor.removeListener(listener);
        assertEquals(2, packetInterceptor.getListenerCount());
        packetInterceptor.removeAllListeners();
        assertEquals(0, packetInterceptor.getListenerCount());
    }

    public void terminateInterceptors() {
        packetInterceptor.terminate();
    }
}
