package au.com.loftinspace.tcpreflector;

import au.com.loftinspace.tcpreflector.test.ConnectionCounterServer;
import au.com.loftinspace.tcpreflector.test.ReverserServer;
import au.com.loftinspace.tcpreflector.test.MessageCountingPacketListener;
import au.com.loftinspace.tcpreflector.test.DestinationCountingPacketListener;
import junit.framework.TestCase;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.ConnectException;
import java.util.List;

/**
 * @author Jem Mawson
 */
public class TcpReflectorTest extends TestCase {
    private ReverserServer reverserServer;
    private Thread tcpReflectorThread;
    private TcpReflector tcpReflector;
    private ConnectionCounterServer connectionCounterServer;

    protected void tearDown() throws Exception {
        if (tcpReflector != null) {
            tcpReflector.shutdown();
        }
        if (tcpReflectorThread != null) {
            tcpReflectorThread.join();
        }
        super.tearDown();
    }

    public void testReflectorConnectivity() throws Exception {
        initialiseReverserServer(8899);

        InetSocketAddress destination = new InetSocketAddress("localhost", reverserServer.getListenPort());
        TcpReflector tcpReflector = new TcpReflector(6346, destination);
        tcpReflectorThread = new Thread(tcpReflector);
        tcpReflectorThread.start();

        connectAndTestReverse();
        tcpReflector.shutdown();
    }

    public void testShutdown() throws Exception {
        testReflectorConnectivity();
        initialiseReverserServer(8899);
        try {
            connectAndTestReverse();
            fail("Expected ConnectException");
        } catch (ConnectException e) {
        }
    }

    public void testReflectiveProperty() throws Exception {

        initialiseReverserServer(8899);
        InetSocketAddress destination = new InetSocketAddress("localhost", reverserServer.getListenPort());
        tcpReflector = new TcpReflector(6346, destination);
        tcpReflectorThread = new Thread(tcpReflector);
        tcpReflectorThread.start();

        // a TcpReflector instance should start life as reflective
        assertTrue(tcpReflector.isReflective());
        connectAndTestReverse();

        // The TcpReflector should not be reflecting connections, so we expect to timeout
        // waiting for response from the ReverserServer.
        tcpReflector.setReflective(false);
        assertFalse(tcpReflector.isReflective());

        initialiseReverserServer(8899);
        Socket socket = new Socket("localhost", 6346);
        socket.setSoTimeout(10000);
        OutputStream output = socket.getOutputStream();
        InputStream input = socket.getInputStream();

        output.write("reverse".getBytes());
        output.flush();
        byte[] response = new byte[7];
        try {
            input.read(response);
            fail("Expected a SocketTimeoutException, but was able to read [" + new String(response) + "]");
        } catch (SocketTimeoutException e) {
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public void testGetAddRemovePacketListeners() throws Exception {
        initialiseReverserServer(8899);
        InetSocketAddress destination = new InetSocketAddress("localhost", reverserServer.getListenPort());
        tcpReflector = new TcpReflector(6346, destination);
        tcpReflectorThread = new Thread(tcpReflector);
        tcpReflectorThread.start();

        // there should be no packet listeners on instantiation
        assertEquals(0, tcpReflector.getPacketListeners().size());

        // create a new listener and add it three times. each time the count should increase.
        MessageCountingPacketListener listener = new MessageCountingPacketListener();
        tcpReflector.addListener(listener);
        assertEquals(1, tcpReflector.getPacketListeners().size());
        tcpReflector.addListener(listener);
        assertEquals(2, tcpReflector.getPacketListeners().size());
        tcpReflector.addListener(listener);
        assertEquals(3, tcpReflector.getPacketListeners().size());

        // an unmodifiable list of listeners should be available
        List listeners = tcpReflector.getPacketListeners();
        assertEquals(3, listeners.size());
        assertEquals(listener, listeners.get(0));
        assertEquals(listener, listeners.get(1));
        assertEquals(listener, listeners.get(2));
        try {
            listeners.remove(0);
            fail("Expected UnsupportedOperationException");
        } catch(UnsupportedOperationException e) {
        }

        // remove the listener instance and the count should decrement
        tcpReflector.removeListener(listener);
        assertEquals(2, tcpReflector.getPacketListeners().size());
        tcpReflector.removeListener(listener);
        assertEquals(1, tcpReflector.getPacketListeners().size());

        // the listener should see both flows of traffic
        initialiseReverserServer(8899);
        connectAndTestReverse();
        listener.assertPacketCount(2, 10000L);

        // the listeners should all be removed
        tcpReflector.removeAllListeners();
        assertEquals(0, tcpReflector.getPacketListeners().size());

        // the listener should no longer record traffic
        listener.resetCount();
        initialiseReverserServer(8899);
        connectAndTestReverse();
        Thread.sleep(2000L);
        assertEquals(0, listener.getPacketCount());
    }

    public void testGetSetDestination() throws Exception {
        initialiseReverserServer(8899);
        InetSocketAddress destination = new InetSocketAddress("localhost", reverserServer.getListenPort());

        tcpReflector = new TcpReflector(6346, destination);
        DestinationCountingPacketListener listener = new DestinationCountingPacketListener();
        tcpReflector.addListener(listener);
        tcpReflectorThread = new Thread(tcpReflector);
        tcpReflectorThread.start();

        // the destination must be the one we constructed it with.
        assertEquals(destination, tcpReflector.getDestination());
        connectAndTestReverse();
        listener.assertPacketCount(destination, 1, 15000L);

        // construct a new destination and assert that the new destination is used.
        // (ReverserServer will only ever service one request).
        initialiseReverserServer(reverserServer.getListenPort() + 1);
        destination = new InetSocketAddress("localhost", reverserServer.getListenPort());
        tcpReflector.setDestination(destination);
        connectAndTestReverse();
        listener.assertPacketCount(destination, 1, 15000L);
        assertEquals(destination, tcpReflector.getDestination());
    }

    public void testGetSetConnective() throws Exception {
        ConnectionCounterServer connectionCounterServer = createCounterServer(8899);
        Thread connectionCounterServerThread = new Thread(connectionCounterServer);
        connectionCounterServerThread.start();

        InetSocketAddress destination = new InetSocketAddress("localhost", connectionCounterServer.getListenPort());
        tcpReflector = new TcpReflector(6346, destination);
        tcpReflectorThread = new Thread(tcpReflector);
        tcpReflectorThread.start();

        // reflector should be connective on instantiation.
        assertTrue(tcpReflector.isConnective());

        // reflector should accept connections.
        new Socket("localhost", 6346);

        tcpReflector.setConnective(false);
        assertFalse(tcpReflector.isConnective());

        // reflector should not accept connections.
        try {
            new Socket("localhost", 6346);
            fail("Expected ConnectException");
        } catch (ConnectException e) {
        }

        // reflector should accept connections once more
        tcpReflector.setConnective(true);
        assertTrue(tcpReflector.isConnective());
        new Socket("localhost", 6346);
    }

    public void testMultipleConnections() throws Exception {
        int connections = 12;

        ConnectionCounterServer connectionCounterServer = createCounterServer(8899);
        Thread connectionCounterServerThread = new Thread(connectionCounterServer);
        connectionCounterServerThread.start();

        InetSocketAddress destination = new InetSocketAddress("localhost", connectionCounterServer.getListenPort());
        tcpReflector = new TcpReflector(6346, destination);
        tcpReflectorThread = new Thread(tcpReflector);
        tcpReflectorThread.start();

        connectToCounterServer(connections);
        Thread.sleep(500);
        connectionCounterServer.terminate();
        connectionCounterServerThread.join();

        assertTrue(connections == connectionCounterServer.getActualConnectionCount());
    }

    private void initialiseReverserServer(int startPort) throws IOException {
        boolean success = false;
        while (!success) {
            try {
                reverserServer = new ReverserServer(++startPort);
                success = true;
            } catch (BindException e) {
            }
        }
        Thread reverserServerThread = new Thread(reverserServer);
        reverserServerThread.start();
    }

    private ConnectionCounterServer createCounterServer(int startPort) throws IOException {
        boolean success = false;
        while (!success) {
            try {
                connectionCounterServer = new ConnectionCounterServer(++startPort);
                success = true;
            } catch (BindException e) {
            }
        }
        return connectionCounterServer;
    }

    private void connectAndTestReverse() throws IOException {
        Socket socket = new Socket("localhost", 6346);
        OutputStream output = socket.getOutputStream();
        InputStream input = socket.getInputStream();

        output.write("reverse".getBytes());
        output.flush();
        byte[] response = new byte[7];
        int bytesRead = input.read(response);
        assertTrue(bytesRead == 7);
        socket.close();
        assertEquals("esrever", new String(response));
    }

    private void connectToCounterServer(int connections) throws IOException {
        for (int i = 0; i < connections; i++) {
            Socket socket = new Socket("localhost", 6346);
            socket.close();
        }
    }
}
