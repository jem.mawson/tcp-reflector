package au.com.loftinspace.utility;

import java.io.FileNotFoundException;

import junit.framework.TestCase;

/**
 * @author Jem
 */
public class FileLoaderTest extends TestCase {
	
	public void testLoadTextFile() throws Exception {
		String contents = FileLoader.loadTextFile("src/test/data/expected-output/au.com.loftinspace.utility.testLoadTextFile.txt");
		assertEquals("Here are the contents of this file", contents);
	}
	
	public void testLoadEmptyTextFile() throws Exception {
		String contents = FileLoader.loadTextFile("src/test/data/expected-output/au.com.loftinspace.utility.testLoadEmptyTextFile.txt");
		assertEquals("", contents);
	}
	
	public void testLoadNonExistentFile() throws Exception {
		try {
			FileLoader.loadTextFile("src/test/data/expected-output/au.com.loftinspace.utility.testLoadNonExistentFile.txt");
			fail("Expected FileNotFoundException");
		} catch (FileNotFoundException e) {
		}
	}
	
	public void testLoadLargeTextFile() throws Exception {
		String contents = FileLoader.loadTextFile("src/test/data/expected-output/au.com.loftinspace.utility.testLoadLargeTextFile.txt");
		StringBuffer buffer = new StringBuffer(50000);
		for (int i = 0; i < 1000; i++) {
			buffer.append("abcdefghijklmnopqrstuvwxyz\nABCDEFGHIJKLMNOPQRSTUV\n");
		}
		assertEquals(buffer.toString(), contents);
	}
}
